//
//  HelloApp.swift
//  Hello
//
//  Created by Eugene Tkachev on 10.04.2023.
//

import SwiftUI

@main
struct HelloApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
